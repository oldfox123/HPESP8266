#ifndef __USART5_H
#define __USART5_H
#include "stdio.h"	
#include "sys.h" 
	
#define UART5_REC_LEN  			200  	//定义最大接收字节数 200
#define UART5_SEND_LEN		600					//最大发送缓存字节数
#define EN_UART5_RX 			1		//使能（1）/禁止（0）串口1接收
	  	
extern u8  UART5_RX_BUF[UART5_REC_LEN]; //接收缓冲,最大USART_REC_LEN个字节.末字节为换行符 
extern u8  UART5_TX_BUF[UART5_SEND_LEN]; 		//发送缓冲,最大USART3_MAX_SEND_LEN字节
extern u16 UART5_RX_STA;         		//接收状态标记	
//如果想串口中断接收，请不要注释以下宏定义
void uart5_init(u32 bound);
void u5_printf(char* fmt,...);
#endif

