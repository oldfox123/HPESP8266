#ifndef __ESP8266_H__
#define __ESP8266_H__	 
#include "sys.h"
//////////////////////////////////////////////////////////////////////////////////	 
#define WIFI_OK 0
#define WIFI_COMMUNTION_ERR 0xff
#define SIM_CPIN_ERR 0xfe
#define WIFI_CONN_FAIL 0xfd
#define SIM_MAKE_CALL_ERR 0Xfc
#define SIM_ATA_ERR       0xfb

#define SIM_CMGF_ERR 0xfa
#define SIM_CSCS_ERR 0xf9
#define SIM_CSCA_ERR 0xf8
#define SIM_CSMP_ERR 0Xf7
#define SIM_CMGS_ERR       0xf6
#define SIM_CMGS_SEND_FAIL       0xf5

#define SIM_CNMI_ERR 0xf4


//extern u8 Flag_Rec_Message;	//收到短信标示
extern char TCP_conn_ok;	  			 //判断GPRS是否链接到服务器了
//extern u8 SIM900_CSQ[3];
extern u8 WIFI_Dect(u8 *SSID,u8 *Password);
extern u8 ESP8266_CONNECT_SERVER_SEND_INFOR(u8 *ClientId,u8 *Username,u8 *Password);
extern u8 ESP8266_CONNECT_SERVER(u8 *ServerIP,u8 *Port);
extern u8 ESP8266_GPRS_SEND_DATA(u8 *temp_data,u8 *Topic);
void sim_at_response(u8 mode);
void check_cmd(void);
u8 ESP8266_send_cmd(u8 *cmd,u8 *ack,u16 waittime);
#endif





