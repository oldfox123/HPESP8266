#include "esp8266.h"
#include "usart.h"		
#include "delay.h"	 
#include "string.h" 
#include "key.h"
#include "usart5.h" 
#include "math.h"
#include "stdio.h"
char TCP_conn_ok=0;	   //GPRS处于连接状态
//********************************************************************************
//无
//////////////////////////////////////////////////////////////////////////////////	
u8 dtbuf[200];   								//打印缓存器	

/**
  * @brief  ESP8266硬件复位
**/
void ESP8266_Rst(void)
{
		GPIO_InitTypeDef GPIO_InitStructure;
    //PD1--对应ESP8266的reset引脚;
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOD, ENABLE);
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_Init(GPIOD, &GPIO_InitStructure);
	
		GPIO_ResetBits(GPIOD,GPIO_Pin_1);
		delay_ms(100);
		GPIO_SetBits(GPIOD,GPIO_Pin_1);
		delay_ms(1000);		
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////// 
//usmart支持部分 
//将收到的AT指令应答数据返回给电脑串口
//mode:0,不清零UART5_RX_STA;
//     1,清零UART5_RX_STA;
void sim_at_response(u8 mode)
{
	if(UART5_RX_STA&0X8000)		//接收到一次数据了
	{ 
		UART5_RX_BUF[UART5_RX_STA&0X7FFF]=0;//添加结束符
		if(strstr((const char*)UART5_RX_BUF,(const char*)"{\"LEDSwitch\":1}"))
		{
			printf("LED灯已打开\r\n");
		}
		else if(strstr((const char*)UART5_RX_BUF,(const char*)"\"LEDSwitch\":0"))
		{
			printf("LED灯已关闭\r\n");
		}
		printf("getdata;%s",(u8 *)UART5_RX_BUF);	//发送到串口
		if(mode)UART5_RX_STA=0;		
	} 
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////// 
//ESP8266发送命令后,检测接收到的应答
//str:期待的应答结果
//返回值:0,没有得到期待的应答结果
//    其他,期待应答结果的位置(str的位置)
u8* ESP8266_check_cmd(u8 *str)
{
	char *strx=0;
	if(UART5_RX_STA&0X8000)		//接收到一次数据了
	{ 
		UART5_RX_BUF[UART5_RX_STA&0X7FFF]=0;//添加结束符
		strx=strstr((const char*)UART5_RX_BUF,(const char*)str);
	} 
	return (u8*)strx;
}

void check_cmd(void)
{

	if(UART5_RX_STA&0X8000)		//接收到一次数据了
	{ 
		UART5_RX_BUF[UART5_RX_STA&0X7FFF]=0;//添加结束符

	} 
		printf("getdata;%s\r\n",UART5_RX_BUF);	//发送到串口

}


//向ESP8266发送命令
//cmd:发送的命令字符串(不需要添加回车了),当cmd<0XFF的时候,发送数字(比如发送0X1A),大于的时候发送字符串.
//ack:期待的应答结果,如果为空,则表示不需要等待应答
//waittime:等待时间(单位:10ms)
//返回值:0,发送成功(得到了期待的应答结果)
//       1,发送失败
u8 ESP8266_send_cmd(u8 *cmd,u8 *ack,u16 waittime)
{
	u8 res=0; 
	UART5_RX_STA=0;
	if((u32)cmd<=0XFF)
	{
		while(DMA1_Channel7->CNDTR!=0);	//等待通道7传输完成   
		UART5->DR=(u32)cmd;
	}else u5_printf("%s\r\n",cmd);//发送命令
				printf("cmd：\r\n %s\r\n",cmd);//发送命令
	if(ack&&waittime)		//需要等待应答
	{
		while(--waittime)	//等待倒计时
		{
			delay_ms(10);
			if(UART5_RX_STA&0X8000)//接收到期待的应答结果
			{
				printf("ATR;%s\r\n",(u8 *)UART5_RX_BUF);	//发送到串口
				if(ESP8266_check_cmd(ack))break;//得到有效数据 
				UART5_RX_STA=0;
			} 
		}
		if(waittime==0)res=1; 
	}
	UART5_RX_STA=0;
	return res;
} 

u8 ESP8266_work_test(u8 *SSID,u8 *Password)
{
   u5_printf("+++");
	delay_ms(1000);
	if(ESP8266_send_cmd((u8 *)"ATE0",(u8 *)"OK",400))
	{
		if(ESP8266_send_cmd((u8 *)"ATE0",(u8 *)"OK",400))return WIFI_COMMUNTION_ERR;	//通信不上
	}		
	ESP8266_send_cmd((u8 *)"AT+CWMODE=3",(u8 *)"OK",400);	//设置WIFI模式
	memset(dtbuf,0,200);
	sprintf((char *)dtbuf,"AT+CWJAP=\"%s\",\"%s\"",SSID,Password);
	if(ESP8266_send_cmd((u8 *)dtbuf,(u8 *)"OK",1000))
	{
		return WIFI_CONN_FAIL;	//等待附着到网络
	}	
	return WIFI_OK;
}
u8 WIFI_Dect(u8 *SSID,u8 *Password)
{
	u8 res;
//	ESP8266_Rst();//物理重启WIFI模块
	delay_ms(1000);
	res=ESP8266_work_test(SSID,Password);	
	switch(res)
	{
		case WIFI_OK:
			printf("WIFI模块自检成功\r\n");
			break;
		case WIFI_COMMUNTION_ERR:
			printf("与WIFI通信失败\r\n");
			break;
		case WIFI_CONN_FAIL:
			printf("连接网络失败\r\n");	
			break;		
		default:
			break;
	}
	return res;
}

u8 ESP8266_CONNECT_SERVER(u8 *ServerIP,u8 *Port)
{	  
		memset(dtbuf,0,200);
		sprintf((char *)dtbuf,"AT+CIPSTART=\"TCP\",\"%s\",%s",ServerIP,Port);
	  if(ESP8266_send_cmd((u8 *)dtbuf,(u8 *)"CONNECT",3000))	return 5;		
		if(ESP8266_send_cmd((u8 *)"AT+CIPMODE=1",(u8 *)"OK",100))	 return 1;
		if(ESP8266_send_cmd((u8 *)"AT+CIPSEND",(u8 *)">",100))	 return 4;
		u5_printf("Hello NNHP \r\n");
		UART5_RX_STA=0;	
	  return 0;                                                            
}
