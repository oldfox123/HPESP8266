#include "delay.h"
#include "usart.h"
#include "usart5.h"
#include "led.h"
#include "oled.h"
#include "bmp.h"
#include "KEY.h"
#include "sys.h" 
#include "esp8266.h" 	
#include "adc.h" 	
#include "dht11.h" 	
#include "string.h"

#define ServerIP "192.168.1.3"
#define Port "9096"
#define SSID "CMCC-IOT"
#define Password "linyi1234567890"
DHT11_Data_TypeDef DHT11_Data;


int main(void)
 {	
	u16 t=0;	
	u8 res=1;	
	u16 adcx;
	float temp; 	 
	delay_init();	    	 //延时函数初始化	 
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2); //设置NVIC中断分组2:2位抢占优先级，2位响应优先级
	uart_init(115200);	 	//串口初始化为115200
	uart5_init(115200);
	LED_Init();			     //LED端口初始化
	KEY_Init();				//按键初始化	
	DHT11_Init();		
	Adc_Init();		  		//ADC初始化
	OLED_Init();			//初始化OLED  
  OLED_Clear();  
	Welcome();
	printf("串口一初始化成功\r\n");
	 while(res)
		{
			res=WIFI_Dect((u8 *)SSID,(u8 *)Password);
			delay_ms(200);
		}	
		res=1;
		while(res)
		{
			res=ESP8266_CONNECT_SERVER((u8 *)ServerIP,(u8 *)Port);
			delay_ms(2000);
		}
		printf("连接服务器成功\r\n");
	while(1) 
	{			
			if(UART5_RX_STA&0X8000)		//接收到数据
			{
				printf("收到服务器数据：\r\n%s\r\n",(const char*)UART5_RX_BUF);
				if(strstr((const char*)UART5_RX_BUF,"LED0K"))
				{
					LED0=0;
				}
				else if(strstr((const char*)UART5_RX_BUF,"LED0G"))
				{
					LED0=1;
				}		
				UART5_RX_STA=0;
			}
			if(USART_RX_STA&0X8000)		//接收到数据
			{
				printf((const char*)USART_RX_BUF,"\r\n");
				u5_printf((char*)USART_RX_BUF);
				USART_RX_STA=0;
			}
		if(t%10==0)			//每1s读取一次
		{									  
			OLED_ShowString(10,2,(u8 *)"voltage:    mV");
			adcx=Get_Adc_Average(ADC_Channel_1,10);
			temp=(float)adcx*(3.3/4096)*100;
			OLED_ShowNum(70,2,temp,4,16);	
      u5_printf("ADC=%d \r\n电压为 %.2f mV \r\n",adcx,temp);	
			printf("ADC=%d \r\n电压为 %.2f mV \r\n",adcx,temp);	
			DHT11_Read_TempAndHumidity (&DHT11_Data );
			delay_ms(1000);
			DHT11_Read_TempAndHumidity (&DHT11_Data );
			u5_printf("读取DHT11成功!\r\n湿度为%d.%d ％RH ，温度为 %d.%d℃ \r\n",\
			DHT11_Data.humi_int,DHT11_Data.humi_deci,DHT11_Data.temp_int,DHT11_Data.temp_deci);
			printf("读取DHT11成功!\r\n湿度为%d.%d ％RH ，温度为 %d.%d℃ \r\n",\
			DHT11_Data.humi_int,DHT11_Data.humi_deci,DHT11_Data.temp_int,DHT11_Data.temp_deci);
			OLED_ShowString(10,4,(u8 *)"HUMI:    %RH");		
			OLED_ShowNum(50,4,DHT11_Data.humi_int,2,16);		
			OLED_ShowString(10,6,(u8 *)"TEMP:   C");		
			OLED_ShowNum(50,6,DHT11_Data.temp_int,2,16);		
		}	
			t++;
			delay_us(100);
			if(t==10000)//大约1s钟改变一次状态
			{
				t=0;				
				LED0=!LED0;
			} 					
		}
}
